## Editando acessos ao SSO

`Menu Gerenciar > Acessos > Botão Editar`

Caso seja necessário, é possível editar a concessão de acesso a um usuário específico. Para tanto, clique no botão **Editar** referente ao item escolhido. Uma nova janela será exibida, trazendo os dados para edição:

<figure style="text-align: center;">
  <img  src="../../img/acessos/10acessos_edit.PNG" width="800"  title="Tela de edição de acesso" />
  <figcaption style="font-size: 14px; font-style: italic; ">Edição de Acesso</figcaption>
</figure>
&nbsp;

1.	**Aplicação**: Selecione a nova aplicação dentre as opções disponíveis;
2.	**Tipo**: Defina o tipo do acesso, se será de *Gestão* ou *Público*;
3.	**Data da expiração**: Defina a data da expiração do acesso;
4.	**Botão Salvar**: Clique no botão **Salvar** para armazenar a atualização;
5.	**Cancelar**: Clique no botão **Cancelar** caso não tenha certeza dos ajustes efetuados.
