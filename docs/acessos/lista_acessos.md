## Acessos

`Menu Gerenciar > Acessos`

Clicando no item **Acessos** do menu *Gerenciar*, a lista de usuários que possuem acesso habilitado às aplicações do SSO é exibida, conforme exemplo:

<figure style="text-align: center;">
  <img  src="../../img/acessos/9_acessos_lista.png" width="800"  title="Lista de Concessões de Acesso" />
  <figcaption style="font-size: 14px; font-style: italic; ">Concessões de Acesso</figcaption>
</figure>
&nbsp;

1.	As colunas exibem o nome da **aplicação**, o nome do **usuário** e a **data** em que expira seu acesso;
2.	**Editar**: Botão de atalho para **editar** o acesso do usuário;
3.	**Barra de Navegação**: Botões de navegação da lista.
