## Lista de usuários

`Menu Gerenciar > Usuários`

É possível cadastrar usuários para utilização direta do sistema SSO, podendo conceder-lhes acessos específicos. Para tanto, o administrador deverá selecionar o item **Usuários** no menu *Gerenciar*.

Clicando no botão, o administrador terá acesso à lista de usuários habilitados no sistema, nome completo, CPF, data do último acesso ao sistema, e ferramentas gerenciais, conforme demonstração:

<figure style="text-align: center;">
  <img  src="../../img/usuarios/50usuario_lista.png" width="800"  title="Lista de usuários" />
  <figcaption style="font-size: 14px; font-style: italic; ">Lista de usuários</figcaption>
</figure>
&nbsp;

1.	**Filtrar por**: Ferramenta que possibilita filtrar a pesquisa de usuários utilizando o parâmetro CPF ou nome.

    1.1 Se o administrador desejar realizar a pesquisa por CPF, deverá selecionar CPF e, no campo buscar, inserir todos os onze dígitos do documento, clicando posteriormente no botão Buscar;

    1.2 Se o administrador deseja realizar a busca por nome, deve selecionar o parâmetro Nome e, no campo buscar, inserir um trecho do nome ou sobrenome desejado, clicando posteriormente em Buscar para realizar a pesquisa;

2.	**Novo Usuário**: Botão de acesso à tela de cadastro de um novo usuário;
3.	**Editar**: Botão de atalho para abrir o formulário de edição de dados pessoais do usuário;
4.	**Informações**: Botão de acesso à área de visualizar detalhes do usuário e outras ferramentas gerenciais;
5.	**Alterar Senha**: Botão de acesso à tela de alteração de senha de um usuário.
