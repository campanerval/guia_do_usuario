## Alteração de senha do usuário

`Menu Gerenciar > Usuários > Botão Alterar Senha`

Nos casos em que o usuário perdeu sua senha de acesso às aplicações, o administrador possui a opção de redefinir a senha, a fim de reabilitar o acesso do usuário. Para tanto, a partir da [**lista principal**](../usuarios/lista_usuarios.md) de usuários do sistema, deverá clicar no botão **Alterar Senha** referente ao usuário que escolhido. Feito isso, o sistema deverá exibir o formulário exemplificado a seguir:

<figure style="text-align: center;">
  <img  src="../../img/usuarios/61usuario_detalhes_reset_senha.png" width="800"  title="Tela de alteração de senha" />
  <figcaption style="font-size: 14px; font-style: italic; ">Tela de alteração de senha</figcaption>
</figure>
&nbsp;

1.	**Senha**: Campo para informar a nova senha do usuário;
2.	**Temporário**: Caixa de seleção que deverá estar desmarcada em caso de usuário que possua acesso por tempo indeterminado às aplicações. Se estiver marcada, o usuário terá um período limitado de acesso às aplicações;
3.	**Validado**: Caixa de seleção informa se o usuário validou seu acesso por meio de confirmação de seu e-mail. Se estiver desmarcada, aponta que o usuário ainda não realizou esta etapa;
4.	**Confirmar Senha**: Campo para confirmar novamente a senha previamente informada;
5.	**Salvar**: Após realizar os ajustes, o administrador deverá clicar no botão **Salvar** para finalizar a redefinição de senha do usuário;
6. **Cancelar**: Caso não tenha certeza dos ajustes realizados, o administrador deverá clicar no botão **Cancelar** para encerrar a operação e retornar à lista geral de usuários.
