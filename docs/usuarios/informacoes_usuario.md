## Ver informações e detalhes de um usuário

`Menu Gerenciar > Usuários > Botão Informações`

Clicando no botão **Informações** referente a um usuário específico, o administrador terá acesso a detalhes de seu cadastro, além de mais ferramentas para complementação de dados e gerenciamento de acesso:

<figure style="text-align: center;">
  <img  src="../../img/usuarios/56usuario_detalhes.png" width="800"  title="Tela de detalhes do usuário" />
  <figcaption style="font-size: 14px; font-style: italic; ">Tela de detalhes do usuário</figcaption>
</figure>
&nbsp;

1.	**Usuário**: A lista destaca os principais dados do usuário, tais como Nome Completo, CPF, data e hora do último login realizado ao sistema, bem como data de registro no sistema e eventual atualização;
2.	**Adicionar Perfil**: Botão de atalho para abrir a tela de **Atribuição de Perfil** ao usuário. 
3.	**Conceder Acesso:** Botão de atalho para abrir a tela de **concessão de acesso** a uma aplicação para o usuário em destaque;
4.	**Concessões de Acesso**: Botão de acesso à lista de aplicações às quais o usuário possui acesso;
5.	**Perfil**: Botão que exibe a lista de perfis do usuário em destaque;
6.	**Lista de Perfis**: Listagem de perfis acumulados de um usuário;
7.	**Voltar**: Botão de retorno à lista principal de usuários;
8.	**Remover**: Botão de atalho para remover um perfil atribuído ao usuário.


