## Atribuindo perfis ao usuário

`Menu Gerenciar > Usuários > Botão Informações > Botão Adicionar Perfil`

É possível atribuir um ou mais perfis a um mesmo usuário, para que este tenha acesso à diferentes seções na mesma aplicação. Para tanto, na tela de **informações do usuário**, o administrador deverá clicar no botão **Adicionar Perfil**, conforme demonstração a seguir:

<figure style="text-align: center;">
  <img  src="../../img/usuarios/57usuario_detalhes_conce_perfil.png" width="800"  title="Adicionando um perfil" />
  <figcaption style="font-size: 14px; font-style: italic; ">Adicionando um perfil</figcaption>
</figure>
&nbsp;

1.	**Adicionar Perfil**: Botão que aciona a tela de seleção de um novo perfil. Um usuário poderá acumular vários perfis de acesso, devendo cada perfil ser definido em operações diferentes. Nesta tela, selecionar o perfil e clicar sobre ele para o sistema registrar a alteração. Como demonstração, atribuiu-se um novo perfil de Usuário;
2.	**Perfil**: Tecla de atalho para listar os perfis do usuário em destaque;
3.	**Usuário**: A lista exibe o perfil Usuário devidamente atribuído.
