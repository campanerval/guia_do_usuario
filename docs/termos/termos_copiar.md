## Copiando um termo pré-existente

`Menu Gerenciar > Termos de Serviço > Botão Copiar`

Para fins de controle e transparência com o usuário, o texto de um termo de serviço não poderá ser editado, mas poderá receber uma nova versão, se necessário. Dependendo do conteúdo a ser alterado, a nova versão deverá ser classificada de acordo com as seguintes nomenclaturas:

* Versão lançada: 0.0.0
* Versão com hotfix: 0.0.1
* Versão com novo paragráfo: 0.1.1
* Versão com muitas alterações: 1.0.0

Desta forma, é possível então criar uma nova versão a partir de um termo já existente. Para tanto, escolha o item que será versionado e clique em seu respectivo botão **Copiar**. O formulário virá preenchido com os dados, possibilitando sua alteração e apontamento do nível da nova versão:

<figure style="text-align: center;">
  <img  src="../../img/termos/14termos__copiar.png" width="600" height="50" title="Copiando um termo" />
  <figcaption style="font-size: 14px; font-style: italic; ">Formulário de criação de termo</figcaption>
</figure>
&nbsp;

Após terminar a edição, clique em **Salvar** para armazenar o novo registro ou em **Cancelar**, se não tiver certeza das atualizações feitas.
