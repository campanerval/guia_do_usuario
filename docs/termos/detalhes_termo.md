## Visualizando detalhes de um termo

`Menu Gerenciar > Termos de Serviço > Botão Informações`

O botão de atalho **Informações**, ao ser clicado, exibe uma janela contendo detalhes gerais do cadastro, como demonstrado abaixo:

<figure style="text-align: center;">
  <img  src="../../img/termos/13termos_detalheEdit.png" width="700"  title="Visualizando detalhes de um termo" />
  <figcaption style="font-size: 14px; font-style: italic; ">Visualização de detalhes do termo</figcaption>
</figure>
&nbsp;

1.	**Caixa de títulos**: Destaque das informações principais do registro efetuado, tais como *versão*, *título*, *status*, *usuário* que criou o termo, *data de adição e atualização*;
2.	**Detalhes**: Clicando no botão **Detalhes**, outra janela é exibida, com destaque para conteúdo do termo de serviço e o título;
3.	**Fechar**: Botão para fechar a janela de detalhes do texto contido no termo de serviço;
4.	**Voltar**: Botão de regresso à lista geral de termos registrados.
