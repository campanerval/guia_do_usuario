## Cadastrando um novo termo de serviço

`Menu Gerenciar > Termos de Serviço > Botão Novo Termo`

Clicando no botão **Novo Termo**, será aberto um formulário para cadastro das informações:

<figure style="text-align: center;">
  <img  src="../../img/termos/12_termos_novo.png" width="700"  title="Tela de cadastro de um novo termo de serviço" />
  <figcaption style="font-size: 14px; font-style: italic; ">Tela de cadastro de um novo termo de serviço</figcaption>
</figure>
&nbsp;

1.	**Título**: Campo para informar o título do termo de serviço;
2.	**Caixa de ferramentas**: botões utilitários para formatação do texto;
3.	**Conteúdo**: Espaço reservado para receber o texto que irá compor o termo;
4.	**Ativo**: Caixa que permite informar se o termo estará ativo ou não;
5.	**Versão**: Campo reservado para inserir a versão do termo. Para termos novos, o código da versão deve ser obrigatoriamente **0.0.0** ;
6.	**Salvar**: Botão para cadastrar o termo de serviço após inserção de todos os dados;
7.	**Cancelar**: Botão que encerra o cadastro do termo e redireciona o usuário à lista.


