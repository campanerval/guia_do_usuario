## Termos de Serviço

`Menu Gerenciar > Termos de Serviço`

Para cadastrar **Termos de Serviços**, o usuário deverá clicar no menu *Gerenciar* e depois no item **Termos de Serviço**. Esta ação irá exibir a tela exemplificada abaixo:

<figure style="text-align: center;">
  <img  src="../../img/termos/11termos__lista.png" width="800"  title="Lista de Termos de Acesso" />
  <figcaption style="font-size: 14px; font-style: italic; ">Lista de Termos de Acesso</figcaption>
</figure>
&nbsp;

1.	**Buscar**: Caixa para pesquisa rápida de termos cadastrados;
2.	**Novo Termo**: Botão de acesso à área de cadastro de termos de serviços;
3.	**Cabeçalho**: A lista traz o *título*, a *versão do termo* e se está *ativo* para exibição;
4.	**Copiar**: Botão de atalho para copiar um termo existente;
5.	**Informações**: Botão para visualizar detalhes do termo de serviço.
