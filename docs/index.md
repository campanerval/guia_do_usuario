# SSO Serviços

## O que é

O SSO é o portal destinado a gerenciar o cadastro de servidores e conceder-lhes permissão de acesso aos serviços do sistema SISECI.

## Meios de acesso

O SSO pode ser acessado por:

* Computador
* Laptop/notebook
* Smartphone

## Formas de Autenticação

Existem duas formas de autenticação: 

* Utilizando o seu login de usuário e senha. O usuário será o CPF informado na criação da conta no SSO;
* Com as credenciais do sistema MT Login.

## Navegador

Todos os navegadores são compatíveis com a Conta SSO. Mas recomenda-se a utilização do Google Chrome e Firefox. 

## Por quem foi criado

O portal SSO foi desenvolvido pela DC4G - *Digital Change For Government*, em parceria com a MTI – *Empresa Mato-grossense de Tecnologia da Informação*.





