## Removendo a associação de um usuário ao órgão

`Menu Gerenciar > Órgãos > Botão Informações > Botão Remover`

Se um usuário for transferido ou desligado de um órgão, é possível remover seu acesso ao órgão ao qual estava habilitado. Clique no botão **Remover** referente ao usuário desejado, conforme exemplificado:

<figure style="text-align: center;">
  <img  src="../../img/orgaos/21orgao_usuario_remover.png" width="700"  title="Remover acesso do usuário ao órgão" />
  <figcaption style="font-size: 14px; font-style: italic; ">Remover acesso do usuário ao órgão</figcaption>
</figure>
&nbsp;

O sistema irá exibir uma nova janela, questionando se o administrador tem certeza e quer concluir a operação. Clique então no botão **Sim** para remover o acesso ao órgão ou clique em **Não**, caso não tenha certeza da operação.