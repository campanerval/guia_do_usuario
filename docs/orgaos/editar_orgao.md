## Editando um órgão existente

`Menu Gerenciar > Órgãos > Botão Editar`

Caso seja necessário, o usuário poderá alterar o *nome* ou a *sigla* do órgão cadastrado. Para tanto, deverá clicar no botão **Editar** relacionado ao registro desejado, para que o sistema traga o formulário com os dados já inseridos para edição:

<figure style="text-align: center;">
  <img  src="../../img/orgaos/17orgaos_editar.png" width="700"  title="Edição de um órgão" />
  <figcaption style="font-size: 14px; font-style: italic;">Edição de um órgão</figcaption>
</figure>
&nbsp;

Após finalizar, o usuário deverá clicar no botão **Salvar** para que o sistema armazene as alterações. Caso não tenha certeza das ações realizadas, o usuário deverá clicar no botão **Cancelar** para que o sistema ignore as alterações.