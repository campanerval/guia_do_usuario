## Visualizando informações detalhadas do órgão

`Menu Gerenciar > Órgãos > Botão Informações`

Para ter acesso a mais detalhes do órgão, o usuário deverá clicar no botão **Informações** referente ao registro desejado. O sistema irá abrir a tela exemplificada a seguir:

<figure style="text-align: center;">
  <img  src="../../img/orgaos/18orgaos_detalhes1.png" width="700"  title="Visualizando detalhes de um órgão" />
  <figcaption style="font-size: 14px; font-style: italic;">Detalhes do órgão</figcaption>
</figure>
&nbsp;

1.	**Informações do órgão**: Esta seção exibe informações cadastrais do órgão, tais como seu nome e sigla, além da data de registro e eventual atualização;
2.	**Usuário**: Botão para acessar o formulário para cadastro de um novo usuário que terá acesso ao referido órgão;
3.	**Associações**: Lista os usuários que possuem acesso ao órgão em destaque;
4.	**Informações**: Botão de atalho para ver detalhes do usuário associado ao órgão (este item será melhor abordado na seção de [*Gerenciamento de Cidadãos*](../cidadaos/detalhes_cidadao.md));
5.	**Editar**: Botão que conduz à tela de edição de acesso do usuário;
6.	**Remover**: Botão que possibilita a exclusão do acesso de um usuário ao órgão em destaque;
7.	**Voltar**: Tecla de retorno à lista de órgãos.
