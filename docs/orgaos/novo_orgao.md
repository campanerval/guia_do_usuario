## Criando um novo órgão no SSO

`Menu Gerenciar > Órgãos > Botão Novo Órgão`

Para criar um órgão no sistema, o usuário deve clicar no botão Novo Órgão, que irá trazer o formulário ilustrado a seguir:

<figure style="text-align: center;">
  <img  src="../../img/orgaos/16orgaos_novo.png" width="700"  title="Criando um novo órgão" />
  <figcaption style="font-size: 14px; font-style: italic; ">Formulário para criação de órgãos</figcaption>
</figure>
&nbsp;

1.	**Sigla**: Campo destinado a receber uma sigla associada ao órgão a ser criado;
2.	**Nome**: Campo para inserir o nome completo do órgão;
3.	**Salvar**: Botão para criar o órgão e finalizar o registro;
4.	**Cancelar**: Botão que encerra a criação e redireciona o usuário à lista de órgãos.
