## Lista geral de órgãos 

`Menu Gerenciar > Órgãos`

Clicando no item **Órgãos** do menu *Gerenciar*, o usuário tem acesso à gerência de autarquias habilitadas no sistema, podendo realizar pesquisas, edição de dados ou cadastro de novos órgãos.

<figure style="text-align: center;">
  <img  src="../../img/orgaos/15orgaos_lista.png" width="700"  title="Lista de órgãos" />
  <figcaption style="font-size: 14px; font-style: italic; ">Lista de órgãos</figcaption>
</figure>
&nbsp;

1.	**Buscar**: Caixa para pesquisa rápida de órgãos cadastrados;
2.	**Novo Órgão**: Botão de acesso à tela de cadastro de novos órgãos;
3.	**Editar**: Tecla de atalho para edição de um órgão específico;
4.	**Informações**: Tecla de atalho para visualizar detalhes de um cadastro.
