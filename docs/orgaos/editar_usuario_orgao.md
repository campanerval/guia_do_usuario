## Editando os dados do usuário de um órgão

`Menu Gerenciar > Órgãos > Botão Informações > Botão Editar`

É possível alterar o perfil de um usuário após concluir seu cadastro. Para tanto, o administrador deverá navegar até seção de **Associações**, presente na tela de **Informações do Órgão**. 


Clique no botão **Editar** referente ao usuário desejado. O sistema deverá exibir uma nova tela, para seleção do novo perfil do usuário:

<figure style="text-align: center;">
  <img  src="../../img/orgaos/20orgao_edit_usuario.png" width="700"  title="Editar perfil de usuário do órgão" />
  <figcaption style="font-size: 14px; font-style: italic;">Alterando perfil de usuário do órgão</figcaption>
</figure>
&nbsp;

1.	**Perfil**: Selecione o perfil desejado dentre as opções disponíveis;
2.	**Salvar**: Clique em **salvar** para armazenar a alteração realizada;
3.	**Cancelar**: Caso não tenha certeza da edição feita, clique em **cancelar** para fechar a janela e retornar à lista de associações.
