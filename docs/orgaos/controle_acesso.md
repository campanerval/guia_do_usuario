# Controles de Acesso

Um órgão poderá ter dois perfis de usuários: **Gerente** e **Coordenador**. 

---

## Gerente

Tem controle sobre tudo que esteja relacionado a um órgão e sistemas clientes, inclusive, é responsável por definir perfis de gerentes e coordenadores no SSO para, por seguinte, definir acesso aos sistemas clientes.

---

## Coordenador

É responsável por uma camada mais específica nos sistemas clientes, não tem poder de efetuar nenhuma mudança no SSO. 

