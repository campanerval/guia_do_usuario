## Habilitar acesso de usuários ao órgão

`Menu Gerenciar > Órgãos > Botão Informações > Botão +Usuário`

Para habilitar o acesso de um usuário ao órgão, o administrador deverá clicar no botão **Usuário**, presente na seção de **Informações do Órgão**. O sistema irá trazer uma nova janela, conforme exemplificado a seguir:

<figure style="text-align: center;">
  <img  src="../../img/orgaos/19orgao__novo_usuario.png" width="700"  title="Habilitando acesso ao órgão" />
  <figcaption style="font-size: 14px; font-style: italic;">Habilitando acesso de usuários ao órgão</figcaption>
</figure>
&nbsp;

1.	**CPF do Usuário**: Neste campo, o usuário administrador deverá inserir ao menos dois dígitos presentes no CPF do novo usuário para que o sistema inicie a pesquisa;
2.	**Perfil**:  Nesta caixa, o administrador deverá atribuir um perfil ao usuário, selecionando entre os opções de [**gerente** ou **coordenador**](../orgaos/controle_acesso.md);
3.	**Salvar**: Após selecionar o CPF e atribuir um perfil ao novo usuário, o administrador deverá clicar em salvar armazenar o cadastro;
4.	**Fechar**: Caso não seja possível terminar a ação, o administrador poderá clicar em fechar, a fim de encerrar a ação.
