## Menu Administrativo
Clicando no botão **Administrativo**, o usuário tem acesso a mais opções e ferramentas gerenciais, além de um dashboard contendo resumo de informações cadastradas:

<figure style="text-align: center;">
  <img  src="../../img/2visaoGeralSSO-Copia.png" width="800"  title="Tela inicial ambiente administrativo" />
  <figcaption style="font-size: 14px; font-style: italic; ">Início - Ambiente Administrativo</figcaption>
</figure>
&nbsp;

1.	**Home:** Botão de retorno à página inicial;
2.	**Aplicações:** Botão de acesso ao cadastro e lista de novos sistemas;
3.	**Gerenciar:** Link contendo atalhos para administração de **Acessos**, **Termos de Serviço**, **Órgãos**, **Cidadãos**, **Tokens**, **E-mails Marketing**, **Chaves de API** e **Usuários**;
4.	**Configurações:** Botão de acesso à página de configuração de permissão de sistemas;
5.	**Grupos:** Acesso à lista de grupos de usuários;
6.	**Nome:** Clicando no nome do usuário, é possível acessar a área de Alteração de Senha;
7.	Caixas contendo números consolidados de **aplicações** disponíveis, **usuários** autenticados no SSO, **pessoas (cidadãos)** e **órgãos** cadastrados.
8.	Links de atalhos às páginas gerenciais de **Aplicações**, **Concessões de Acessos**, **Órgãos** e **Cidadãos**.

