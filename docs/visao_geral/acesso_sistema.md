## Acesso ao sistema SSO Serviços

Para acessar o sistema SSO, o usuário deve inserir o seguinte endereço no seu navegador de preferência: [https://sso.servicos.mt.gov.br/login/](https://sso.servicos.mt.gov.br/login/?next=/)
Será exibida a home page do sistema, com o formulário de acesso. Inserindo os dados CPF e senha nos campos apropriados e clicando posteriormente no botão **Entrar**, o usuário poderá autenticar-se no sistema:

<figure style="text-align: center;">
  <img  src="../../img/0.7login.png" width="700"  title="Tela de Login" />
  <figcaption style="font-size: 14px; font-style: italic; ">Tela de Login</figcaption>
</figure>
&nbsp;