# Início
Após realizar a autenticação no sistema, o usuário terá acesso à página inicial exemplificada abaixo: 

<figure style="text-align: center;">
  <img  src="../../img/1visaoGeralSSO.png" width="800"  title="Tela inicial do SSO" />
  <figcaption style="font-size: 14px; font-style: italic; ">Tela inicial do SSO</figcaption>
</figure>
&nbsp;

1.	**Home:** Âncora de retorno ao início do sistema;
2.	**Administrativo:** Botão de acesso às ferramentas de administração geral;
3.	**Nome:** Clicando no nome do usuário, a opção de Sair do sistema fica disponível;
4.	**Minhas informações:** Exibe o nome completo do usuário, seu CPF e seu RG;
5.	**Histórico de Acessos:** Relatório dos últimos acessos realizados ao SSO;
6.	**Alterar Senha:** Botão de acesso à página de atualização da senha pessoal;
7. **Portal de Serviços do Cidadão:** Link de acesso ao portal público de serviços.
