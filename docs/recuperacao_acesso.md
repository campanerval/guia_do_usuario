
# Como recuperar senha de acesso

## Iniciar recuperação de acesso

1- O usuário deverá acessar o [sistema SSO](https://sso.servicos.mt.gov.br/login/?next=/)  e, na tela de autenticação, clicar em **“Esqueceu sua senha?”**

<figure style="text-align: center;">
  <img  src="../img/0.1recuperaSenha1.png" width="300" title="Tela de login" />
  <figcaption style="font-size: 14px; font-style: italic; ">Tela de login</figcaption>
</figure>
&nbsp;

---

## Confirmar dados 

 2- Na próxima tela, deverá inserir os dados solicitados, sendo seu **CPF** e **Data de Nascimento**, clicando depois no botão **Enviar**.

<figure style="text-align: center;">
  <img  src="../img/0.2recuperaSenha2.png" width="300" title="Confirmação de dados" />
  <figcaption style="font-size: 14px; font-style: italic; ">Tela de confirmação de dados</figcaption>
</figure>
&nbsp;

---

## Verificar e-mail para redefinição da senha

3- Caso tenha inserido as informações corretamente, o sistema deverá exibir a seguinte mensagem:

<figure style="text-align: center;">
  <img  src="../img/0.3recuperaSenha3.png" width="400" title="Mensagem" />
  <figcaption style="font-size: 14px; font-style: italic; ">Mensagem</figcaption>
</figure>
&nbsp;

---

O usuário deverá checar em sua caixa de entrada o e-mail enviado e clicar no link para iniciar o processo de redefinição de senha:
<figure style="text-align: center;">
  <img  src="../img/0.4recuperaSenha4.png" width="800"  title="E-mail enviado ao usuário" />
  <figcaption style="font-size: 14px; font-style: italic; ">E-mail enviado ao usuário</figcaption>
</figure>
&nbsp;

---

## Definir nova senha

4- O link deverá conduzir o usuário a tela de **Alterar Senha**, conforme demonstração:

<figure style="text-align: center;">
  <img  src="../img/0.5recuperaSenha5.png" width="300"  title="Alteração de senha" />
  <figcaption style="font-size: 14px; font-style: italic; ">Tela de alteração de senha</figcaption>
</figure>
&nbsp;

---

O usuário deverá então informar uma senha contendo ao menos 8 caracteres e obrigatoriamente repeti-la no campo Confirmar Senha, clicando por fim no botão **Alterar senha**.
Caso tudo ocorra como esperado, o sistema deverá exibir a seguinte mensagem de sucesso:

<figure style="text-align: center;">
  <img  src="../img/0.6recuperaSenha6.png" width="300"  title="Mensagem de sucesso" />
  <figcaption style="font-size: 14px; font-style: italic; ">Mensagem de sucesso para redefinição de senha</figcaption>
</figure>
&nbsp;

---

5- O usuário poderá então clicar no botão **Efetuar Login** para ser redirecionado à tela de acesso ao SSO ou clicar no link **Ir ao portal do cidadão**.
