# Adicionando e visualizando endereços do cidadão

## Inserindo endereço

`Menu Gerenciar > Cidadãos > Botão Informações > Botão Adicionar Endereço`

Para inserir o endereço de um usuário, estando na tela de **visualização de detalhes**, o administrador deverá clicar no botão **Adicionar Endereço** para acessar este formulário, conforme demonstração:

<figure style="text-align: center;">
  <img  src="../../img/cidadaos/32cidadao_detalhes_cad_endereco.png" width="300"  title="Cadastrando endereço do usuário" />
  <figcaption style="font-size: 14px; font-style: italic;">Cadastrando endereço do usuário</figcaption>
</figure>
&nbsp;


1. **Endereço**: Campo obrigatório para inserir o nome do logradouro e número residencial do usuário;
2. **Complemento**: Campo para inserir eventual complemento de endereço;
3.	**Bairro**: Campo obrigatório para inserir o bairro de residência do usuário;
4.	**Cidade**: Campo obrigatório para selecionar a lista a cidade de residência do usuário;
5.	**Cep**: Campo para inserir o número do CEP residencial do usuário;
6.	**Endereço atual**: Caixa de seleção para destacar se o endereço informado pelo usuário é o atual;
7.	**Observações**: Campo para inserir alguma informação pertinente ao endereço do usuário;
8.	**Zona de localização**: Campo para selecionar a área mais adequada ao endereço do usuário, se *rural* ou *urbana*;
9.	**Salvar**: Após inserir dados de endereço informados pelo usuário, o administrador deverá clicar em **Salvar** para que o sistema efetue o registro;
10.	**Fechar**: Caso não seja possível concluir a operação, o administrador deverá clicar no botão **Fechar** para retornar à tela de informações do usuário.

---

## Visualizando o endereço

`Menu Gerenciar > Cidadãos > Botão Informações > Botão Endereços`

Após adicionar o endereço do usuário, será possível visualizar um resumo desses dados ainda na tela de **informações**, acionando o botão **Endereços**, conforme exemplo:

<figure style="text-align: center;">
  <img  src="../../img/cidadaos/32.1cidadao_detalhes_cad_endereco.png" width="800"  title="Visualização de endereço" />
  <figcaption style="font-size: 14px; font-style: italic;">Visualização de endereço</figcaption>
</figure>
&nbsp;

1.	**Endereços**: Botão de atalho para destacar endereços do usuário;
2.	**Títulos**: A lista destaca os principais dados do endereço, bem como se é o endereço atual;
3.	**Informações**: Botão que aciona uma nova tela, exibindo mais detalhes do endereço, além da data de criação e atualização;
4.	**Fechar**: Botão para **fechar** a tela de detalhes do endereço;
5.	**Editar**: Botão que aciona o formulário para **editar** um endereço selecionado;
6.	**Voltar**: Botão de retorno à lista geral de usuários cadastrados.

---

## Editando um endereço

`Menu Gerenciar > Cidadãos > Botão Informações > Botão Endereços > Botão Editar`

Caso seja necessário alterar o endereço do usuário, o administrador deverá clicar no botão **Editar** associado ao endereço que precisa ser atualizado. Feito isso, o sistema irá trazer o formulário com todos os dados existentes, disponíveis para edição: 

<figure style="text-align: center;">
  <img  src="../../img/cidadaos/32.2cidadao_detalhes_edit_endereco.png" width="800"  title="Edição de um endereço" />
  <figcaption style="font-size: 14px; font-style: italic;">Edição de um endereço</figcaption>
</figure>
&nbsp;

Após atualizar o necessário, o administrador deverá clicar no botão **Salvar** para que as alterações sejam registradas no sistema. Caso não tenha certeza dos ajustes realizados, o administrador deverá clicar em **Cancelar** para encerrar a operação e ser redirecionado à lista de usuários.