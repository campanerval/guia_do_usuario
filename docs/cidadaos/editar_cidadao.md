## Editando dados do cidadão

`Menu Gerenciar > Cidadãos > Botão Editar`

Caso seja necessário alterar algum dado cadastral de usuário, o administrador deverá clicar no botão **Editar** associado ao item escolhido. Feito isso, o sistema irá trazer o formulário com todos os dados existentes, disponíveis para edição. No exemplo a seguir, apenas duas seções do formulário seguem ilustradas, para fins de demonstração:

<figure style="text-align: center;">
  <img  src="../../img/cidadaos/26cidadao_editar.png" width="700"  title="Formulário de ediçao do usuário" />
  <figcaption style="font-size: 14px; font-style: italic;"></figcaption>
</figure>
<figure style="text-align: center;">
  <img  src="../../img/cidadaos/27cidadao_editar2.png" width="700"  title="Formulário de ediçao do usuário" />
  <figcaption style="font-size: 14px; font-style: italic;">Formulário de edição do usuário</figcaption>
</figure>
&nbsp;

Após realizar os ajustes necessários no cadastro do usuário, o administrador deverá clicar no botão **Salvar** para que as alterações sejam registradas no sistema. Caso não tenha certeza dos ajustes realizados, o administrador deverá clicar em **Cancelar** para encerrar a operação e ser redirecionado à lista de usuários.