## Informando o óbito de um usuário 

`Menu Gerenciar > Cidadãos > Botão Informações > Botão Informar Óbito`

Em caso de ocorrência conhecida de óbito do usuário, será possível formalizar isso no sistema, de modo que seu acesso seja cancelado. Para tanto, o administrador deverá clicar no botão **Informar Óbito**, presente na tela de **informações** e detalhes do **usuário**. O sistema irá exibir o formulário para relatar detalhes da ocorrência, conforme exemplo:

<figure style="text-align: center;">
  <img  src="../../img/cidadaos/38cidadao_detalhes_obito.png" width="700"  title="Tela para informar óbito" />
  <figcaption style="font-size: 14px; font-style: italic;">Tela para informar óbito</figcaption>
</figure>
&nbsp;

1.	**Óbito**: caixa de seleção que registra o óbito do usuário, caso esteja marcada;
2.	**Data do óbito**: Campo para informar a data da ocorrência do óbito;
3.	**Motivo do óbito**: Campo para informar a causa do falecimento do usuário, se tiver sido informada;
4.	**Salvar**: Após registrar os dados solicitados, o administrador deverá clicar no botão **Salvar** para que o sistema registre a ocorrência;
5.	**Cancelar**: Caso não tenha certeza das informações, o administrador deverá clicar no botão **Cancelar** para fechar o formulário e retornar à tela de informações do usuário.
