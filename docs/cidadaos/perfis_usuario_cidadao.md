# Visualizando detalhes de perfil do usuário

## Detalhes do perfil

`Menu Gerenciar > Cidadãos > Botão Informações > Botão Perfil de Usuário`

Assim que o usuário estiver habilitado em alguma aplicação, será possível **visualizar detalhes** de seu perfil de usuário, conforme demonstração:

<figure style="text-align: center;">
  <img  src="../../img/cidadaos/36cidadao_detalhes_perfil.png" width="700"  title="Detalhes de perfil do usuário" />
  <figcaption style="font-size: 14px; font-style: italic;">Detalhes de perfil do usuário</figcaption>
</figure>
&nbsp;

1.	**Perfil de Usuário:** Botão de atalho para exibir detalhes e informações de acesso do usuário;
2.	**Informações de Acesso:** O resumo destaca o tipo do usuário, se seu acesso está ativo, data de recorrência de acesso e data de criação;
3.	**Editar**: Botão que aciona tela para realizar ajustes complementares no perfil do usuário;
4.	**Resetar Senha**: Botão para abrir formulário de ajuste na senha de acesso do usuário às aplicações;
5.	**Voltar**: Botão de retorno à lista geral de usuários cadastrados.

---

## Editando perfil de acesso

`Menu Gerenciar > Cidadãos > Botão Informações > Botão Perfil de Usuário > Botão Editar`

Para realizar ajustes no perfil de acesso do usuário, na lista de **Perfil de Usuário**, o administrador deverá clicar no botão **Editar**, que irá abrir a tela contendo o formulário com caixas de seleção previamente marcadas, conforme exemplo:

<figure style="text-align: center;">
  <img  src="../../img/cidadaos/37cidadao_detalhes_perfil_edit.png" width="700"  title="Editar perfil do usuário" />
  <figcaption style="font-size: 14px; font-style: italic;">Editar perfil do usuário</figcaption>
</figure>
&nbsp;

1.	**Staff status**: A caixa de seleção deverá estar marcada apenas para usuários da equipe administrativa do SSO;
2.	**Active**: Caixa de seleção que, se estiver marcada, atribui status de acesso ativo ao usuário. Estando desmarcada, o usuário não poderá acessar utilizando seus dados de acesso;
3.	**Temporário**: Caixa de seleção que deverá estar desmarcada em caso de usuário que possua acesso por tempo indeterminado às aplicações. Se estiver marcada, o usuário terá um período limitado de acesso às aplicações;
4.	**Validado**: Caixa de seleção informa se o usuário validou seu acesso por meio de confirmação de seu e-mail. Se estiver desmarcada, aponta que o usuário ainda não realizou esta etapa; 
5.	**Salvar**: Após realizar os ajustes necessários, o administrador deverá clicar no botão **Salvar** para armazenar a atualização realizada;
6.	**Cancelar**: Caso não tenha certeza dos ajustes feitos, o administrador deverá clicar no botão **Cancelar** para retornar à tela de informações do usuário.



