## Visualizando detalhes de um cidadão

`Menu Gerenciar > Cidadãos > Botão Informações`

Clicando no botão **Informações** referente a um usuário específico, o administrador terá acesso a detalhes de seu cadastro, além de mais ferramentas para complementação de dados e gerenciamento de acesso:

<figure style="text-align: center;">
  <img  src="../../img/cidadaos/28cidadao__detalhes1.png" width="600"  title="Informações do usuário" />
  <figcaption style="font-size: 14px; font-style: italic;">Informações do usuário</figcaption>
</figure>
&nbsp;
<ol start="1">
  <li><b>Coluna de títulos:</b> Traz os principais dados do usuário, além da data de criação do cadastro e data de eventual atualização realizada;</li>
  <li> <b>Adicionar Contato:</b> Botão para acesso ao formulário de cadastro de informações complementares de contato do usuário;</li>
  <li> <b>Adicionar Endereço:</b> Botão de acesso ao formulário para cadastrar o endereço do usuário;</li>
  <li> <b>Informar Óbito:</b>  Botão que aciona a tela para relatar a ocorrência de óbito do usuário;</li>
  <li> <b>Conceder Acesso:</b> Botão que exibe o formulário que possibilita conceder acessos ao usuário;  </li>
</ol>

<figure style="text-align: center;">
  <img  src="../../img/cidadaos/29cidadao_detalhes2.png" width="700"  title="Botões adicionais de configuração do usuário" />
  <figcaption style="font-size: 14px; font-style: italic;">Botões adicionais de configuração do usuário</figcaption>
</figure>
&nbsp;
<ol start="6">
  <li><b>Endereços:</b> Botão que lista os endereços do usuário; </li>
  <li><b>Contatos:</b> Botão que lista os contatos do usuário;</li>
  <li><b>Perfil de Usuário:</b> Botão que exibe detalhes do perfil do usuário, além de ferramentas de edição de acesso e redefinição de senha;</li>
  <li><b>Concessões de Acesso:</b> Botão que lista as aplicações que o usuário possui acesso;</li>
  <li><b>Voltar: </b> Botão de retorno à lista de usuários.</li>  
</ol>

