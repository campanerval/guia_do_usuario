# Adicionando e visualizando contatos do cidadão

## Inserindo dados

`Menu Gerenciar > Cidadãos > Botão Informações > Botão Adicionar Contato`

Para inserir dados de contato de um usuário, estando na tela de visualização de detalhes, o administrador deverá clicar no botão **Adicionar Contato**, de modo a acessar o respectivo formulário conforme demonstração:

<figure style="text-align: center;">
  <img  src="../../img/cidadaos/30cidadao_detalhes_cad_contato.png" width="300"  title="Cadastrando contatos do usuário" />
  <figcaption style="font-size: 14px; font-style: italic;">Cadastrando contatos do usuário</figcaption>
</figure>
&nbsp;

1.	**Email**: Campo para inserir um endereço de e-mail do usuário;
2.	**Telefone**: Campo para inserir o telefone residencial;
3.	**Celular**: Campo para inserir um número de telefone móvel do usuário;
4.	**Whatspapp**: Campo para inserir o número de whatsapp do usuário;
5.	**Twitter**: Campo para inserir a conta do usuário na rede social Twitter;
6.	**Facebook**: Campo para inserir a conta do usuário na rede social Facebook;
7.	**Instagram**:  Campo para inserir a conta do usuário na rede social Instagram;
8.	**Salvar**: Após encerrar o cadastro de dados, o administrador deverá clicar no botão **Salvar** para o sistema armazenar as informações;
9.	**Fechar**: Caso não seja possível concluir o preenchimento do formulário, basta clicar no botão **Fechar** para retornar à tela de informações do usuário.

## Visualizando dados

`Menu Gerenciar > Cidadãos > Botão Informações > Botão Contatos`

Após adicionar informações de contato do usuário, será possível visualizar um resumo desses dados ainda na tela de informações, acionando o botão **Contatos**, conforme demonstração:

<figure style="text-align: center;">
  <img  src="../../img/cidadaos/31cidadao_detalhes_cad__contato.png" width="700"  title="Visualizando contatos do usuário" />
  <figcaption style="font-size: 14px; font-style: italic;">Visualizando contatos do usuário</figcaption>
</figure>
&nbsp;

1.	**Contatos**: Botão de atalho para destacar contatos do usuário;
2.	**Títulos**: A lista destaca os principais contatos do usuário, bem como alerta se o e-mail foi validado;
3.	**Informações**: Botão que aciona uma nova tela, exibindo todos os contatos existentes do usuário, além da data de criação e atualização;
4.	**Fechar**: Botão para fechar a tela de detalhes de contatos;
5.	**Editar**: Botão que aciona o formulário para editar os contatos exibidos na tela de **informações**;
6.	**Voltar**: Botão de retorno à lista geral de usuários cadastrados.

## Editando dados de contato

`Menu Gerenciar > Cidadãos > Botão Informações > Botão Contatos > Botão Editar`

Caso seja necessário alterar contatos do usuário, o administrador deverá clicar no botão **Editar** associado ao item escolhido. Feito isso, o sistema irá trazer o formulário com todos os dados existentes, disponíveis para edição:

<figure style="text-align: center;">
  <img  src="../../img/cidadaos/31.1cidadao_detalhes_edit_contato.png" width="700"  title="Editando dados de contatos do usuário" />
  <figcaption style="font-size: 14px; font-style: italic;">Tela de edição de contato do usuário</figcaption>
</figure>
&nbsp;

Após realizar os ajustes necessários, o administrador deverá clicar no botão **Salvar** para que as alterações sejam registradas no sistema. Caso não tenha certeza dos ajustes realizados, o administrador deverá clicar em **Cancelar** para encerrar a operação e ser redirecionado à lista de usuários.