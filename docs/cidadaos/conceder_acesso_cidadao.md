# Concedendo acessos ao usuário

`Menu Gerenciar > Cidadãos > Botão Informações > Botão Conceder Acesso`

## Conceder um acesso

Para permitir o acesso do usuário a um sistema específico, estando na tela de visualização de detalhes, o administrador deverá clicar no botão **Conceder Acesso**, de modo a acessar formulário de concessão, conforme demonstração:

<figure style="text-align: center;">
  <img  src="../../img/cidadaos/33.1cidadao_detalhes_conced_acesso.png" width="400"  title="Tela de concessáo de acesso" />
  <figcaption style="font-size: 14px; font-style: italic;">Concessão de acesso</figcaption>
</figure>
&nbsp;

---

1.	**Aplicação**: Campo obrigatório para selecionar aplicação à qual deseja conceder acesso ao usuário;
2.	**Tipo**: Campo obrigatório para selecionar o perfil de acesso que será atribuído ao usuário na aplicação escolhida;
3.	**Salvar**: Tendo finalizado o cadastro, o administrador deverá clicar no botão **Salvar** para armazenar o registro;
4.	**Fechar**: Caso não seja possível concluir a operação, o administrador deverá clicar no botão **Fechar** para retornar à tela de informações do usuário.

## Listando acessos

`Menu Gerenciar > Cidadãos > Botão Informações > Botão Concessões de Acesso`

Após conceder ao usuário acesso a um determinado aplicativo, será possível visualizar uma lista dos sistemas habilitados na parte debaixo tela de informações, acionando o botão **Concessões de Acesso**, conforme demonstração:

<figure style="text-align: center;">
  <img  src="../../img/cidadaos/34cidadao_detalhes_conced_acesso.png" width="700"  title="Lista de acessos permitidos ao usuário" />
  <figcaption style="font-size: 14px; font-style: italic;">Acessos concedidos</figcaption>
</figure>
&nbsp;

--- 

1.	**Concessões de Acesso**: Botão de atalho para exibir a lista de aplicativos aos quais o usuário possui acesso;
2.	**Títulos**: A lista destaca o nome das aplicações;
3.	**Editar**: Botão de acesso à tela de edição do acesso à uma aplicação;
4.	**Remover**: Botão que permite remover o acesso do usuário à aplicação referenciada;
5.	**Voltar**: Botão de retorno à lista geral de usuários cadastrados.

## Editando acessos

`Menu Gerenciar > Cidadãos > Botão Informações > Botão Concessões de Acesso > Botão Editar`

Para realizar alguma alteração na concessão de acesso a uma aplicação, na lista de Concessões de Acesso, o administrador deverá clicar no botão **Editar**, que irá abrir a tela contendo o formulário com os dados já preenchidos:


<figure style="text-align: center;">
  <img  src="../../img/cidadaos/35.1cidadao_detalhes_conced_acesso_edit.png" width="700"  title="Tela de edição de acessos do usuário" />
  <figcaption style="font-size: 14px; font-style: italic;">Edição de acessos do usuário</figcaption>
</figure>
&nbsp;

Será possível então realizar alterações na aplicação definida e trocar o perfil do acesso. Após concluir os ajustes, o administrador deverá clicar em **Salvar** para armazenar as mudanças ou clicar em **Cancelar**, caso não se tenha certeza dos ajustes efetuados.