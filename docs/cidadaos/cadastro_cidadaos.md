## Cadastro de cidadãos

`Menu Gerenciar > Cidadãos > Botão Novo Cadastro`

Para cadastrar um novo usuário, o administrador deverá clicar no botão **Novo Cadastro**, disponível no topo da listagem de cidadãos. Após clicar, o sistema deverá trazer o formulário exemplificado a seguir:

<figure style="text-align: center;">
  <img  src="../../img/cidadaos/23cidadao_cadastrar1.png" width="700"  title="Formulário de cadastro do cidadão" />
  <figcaption style="font-size: 14px; font-style: italic;"></figcaption>
</figure>
<figure style="text-align: center;">
  <img  src="../../img/cidadaos/24cidadao_cadastrar2.png" width="700"  title="Formulário de cadastro do cidadão />
  <figcaption style="font-size: 14px; font-style: italic;"></figcaption>
</figure>
<figure style="text-align: center;">
  <img  src="../../img/cidadaos/25cidadao_cadastrar3.png" width="700"  title="Formulário de cadastro do cidadão" />
  <figcaption style="font-size: 14px; font-style: italic;">Formulário de cadastro do cidadão</figcaption>
</figure>
&nbsp;

1.	**Nome**: Campo obrigatório para inserir o nome completo do usuário, conforme consta no RG.;
2.	**Nome Social**: Campo para inserir o nome pelo qual o usuário deseja ser chamado;
3.	**Identificação Social**: Campo utilizado para informar se o usuário faz parte de algum grupo que possua características de seu interesse;
4.	**CPF**: Campo obrigatório no qual devem ser inseridos apenas os números do CPF do usuário;
5.	**CNS**: Campo para informar a numeração do usuário no Cadastro Nacional de Saúde;
6.	**RG**: Campo para informar a numeração do registro geral ou identidade do usuário;
7.	**UF**: Campo para selecionar a Unidade Federal de origem do RG do usuário;
8.	**Órgão Expedidor**: Campo para informar em qual órgão foi emitido o RG do usuário;
9.	**Estado Civil**: Campo para selecionar da lista o estado civil informado pelo usuário;
10.	**Raça/Cor**: Campo para selecionar da lista a cor de pele com a qual o usuário se identifica;
11.	**Deficiente**: Caixa para ser marcada caso o usuário relate alguma deficiência;
12.	**Tipo da deficiência:** Campo para relatar qual o tipo da deficiência informada pelo usuário;
13.	**Nacionalidade:** Campo para informar a nacionalidade do usuário, a partir de seu país de origem;
14.	**Naturalidade:** Campo para informar a naturalidade do usuário, a partir de sua cidade de nascimento;
15.	**UF:** Campo para selecionar o Estado no qual nasceu o usuário, em caso de nacionalidade brasileira;
16.	**Sexo:** Campo para selecionar da lista o sexo/gênero com o qual o usuário se identifica;
17.	**Nome do pai:** Campo para informar o nome do pai do usuário;
18.	**Nome da mãe:** Campo obrigatório para informar o nome da mãe do usuário. Caso o usuário não saiba, deve ser mantida a informação “Não Consta”, pré-existente no respectivo campo;
19.	**Responsável:** Campo para informar o nome do responsável legal pelo usuário, caso exista;
20.	**Data de Nascimento:** Campo para informar a data de nascimento do usuário;
21.	**Salvar:** Após inserir todos os dados do usuário ou ao menos os classificados como obrigatórios, o administrador deverá clicar em Salvar para que o sistema registre o usuário;
22. **Cancelar:** Caso não seja possível finalizar o cadastro, o administrador poderá encerrar a operação clicando no botão Cancelar.
