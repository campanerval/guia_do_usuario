## Lista de usuários / cidadãos

`Menu Gerenciar > Cidadãos`

O item **Cidadãos** do menu *Gerenciar* é a seção destinada a habilitação edição de usuários que irão utilizar o sistema. Clicando neste item, será exibida uma lista de cidadãos cadastrados e demais ferramentas gerenciais, conforme exemplo:

<figure style="text-align: center;">
  <img  src="../../img/cidadaos/22cidadaos_lista.png" width="700"  title="Lista de cidadãos" />
  <figcaption style="font-size: 14px; font-style: italic;">Lista de cidadãos</figcaption>
</figure>
&nbsp;

1.	**Novo cadastro**: Botão de acesso à tela de cadastro de novos usuários do sistema;
2.	**Filtrar por**: Ferramenta que possibilita filtrar a pesquisa de usuários utilizando o parâmetro **CPF** ou **nome**.

    2.1 Se o usuário desejar realizar a pesquisa por CPF, deverá selecionar CPF e, no campo **buscar**, inserir todos os onze dígitos do documento, clicando posteriormente no botão **Buscar**;

    2.2 Se o usuário deseja realizar a busca por nome, deve selecionar o parâmetro **Nome** e, no campo **buscar**, inserir um trecho do nome ou sobrenome desejado, clicando posteriormente em **Buscar** para realizar a pesquisa;

3.	**Buscar**: Caixa para pesquisa rápida de usuários cadastrados, utilizada juntamente com a ferramenta de filtro;
4.	**Navegação**: Botões de navegação para acessar diversas partes da lista de usuários;
5.	**Editar**: Botão de atalho para o formulário de edição de dados do usuário;
6.	**Informações**: Botão de atalho para tela de visualização de informações do usuário.
