# Criando e editando email marketing

## Email marketing

`Menu Gerenciar > Email Marketing`

Para enviar emails em massa aos usuários, o administrador deverá escolher o item **Email Marketing** no menu *Gerenciar*. Ao clicar no item, o sistema irá exibir uma lista de emails já enviados, relacionando o **título do email**, o **autor**, **status** de envio e sua respectiva **data**, conforme exemplo a seguir:

<figure style="text-align: center;">
  <img  src="../../img/emails/43email_lista.png" width="700"  title="Lista de emails marketing" />
  <figcaption style="font-size: 14px; font-style: italic;">Lista de emails marketing</figcaption>
</figure>
&nbsp;

1.	**Buscar**: Caixa para pesquisa rápida de emails enviados;
2.	**Novo Email**: Botão de acesso à tela de redação de um novo email;
3.	**Editar**: Botão de acesso ao formulário de edição do email ainda não enviado;
4.	**Informações**: Botão de acesso à área de visualização de detalhes de um email;

---

## Criar um novo email marketing

`Menu Gerenciar > Email Marketing > Botão Novo Email`

Para redigir um novo email marketing, o usuário deverá clicar no botão **Novo Email**, presente na tela de listagem de emails. O sistema deverá exibir o formulário para inserir o conteúdo e programar a data do envio, conforme demonstração:

<figure style="text-align: center;">
  <img  src="../../img/emails/44email_novo.png" width="700"  title="Criando um novo email marketing" />
  <figcaption style="font-size: 14px; font-style: italic;">Criando um novo email marketing</figcaption>
</figure>
&nbsp;

1.	**Título**: Campo para informar o título do email;
2.	**Caixa de ferramentas**: Caixa de botões utilitários para formatação do conteúdo do email;
3.	**Conteúdo**: Campo para inserir o texto do email marketing;
4.	**Ativo**: Caixa selecionável, que permite informar se o envio do email está ativo ou não;
5.	**Data para envio**: Campo para inserir a data em que o email marketing será enviado;
6.	**Hora para envio**: Campo para inserir o horário em que o email marketing será enviado;
7.	**Salvar**: Após concluir a redação do email, o usuário deverá clicar no botão **Salvar** para que o sistema armazene o novo registro;
8.	**Cancelar**: Caso não seja possível concluir a redação, o usuário deverá clicar no botão **Cancelar** para retornar à lista de emails.

---

## Editando um email antes do envio

`Menu Gerenciar > Email Marketing > Botão Editar`

Antes de ocorrer o envio do email, o usuário poderá alterar o seu conteúdo conforme necessário. Para tanto, deverá clicar no botão **Editar** relacionado ao email desejado, para que o sistema exiba o formulário com os dados previamente inseridos:

<figure style="text-align: center;">
  <img  src="../../img/emails/45email_edit.png" width="700"  title="Editando um email marketing" />
  <figcaption style="font-size: 14px; font-style: italic;">Editando um email marketing</figcaption>
</figure>
&nbsp;

Após editar e terminar os ajustes, o usuário deverá clicar no botão **Salvar** para que o sistema capture e armazene as alterações no email. Caso o usuário não tenha certeza das ações realizadas, deverá clicar no botão **Cancelar**, a fim de retornar à listagem de emails.

---

## Visualizando detalhes de um email

`Menu Gerenciar > Email Marketing > Botão Informações`

Após redigir e salvar um email e independente de seu status de envio, será possível visualizar detalhes do registro. Para tanto, o usuário deverá clicar no botão **Informações** associado ao email escolhido. Clicando neste botão, o sistema irá exibir uma janela de detalhes do email, como segue:

<figure style="text-align: center;">
  <img  src="../../img/emails/46email_informacoes.png" width="700"  title="Visualizando um email marketing" />
  <figcaption style="font-size: 14px; font-style: italic;">Visualizando um email marketing</figcaption>
</figure>
&nbsp;

1.	**Informações do email**: Aparecem destacados na tela o título do email, status de envio, data e hora de envio, o nome do autor, data da criação e eventual atualização;
2.	**Detalhes**: Botão que aciona uma nova janela, exibindo o título e o conteúdo do email;
3.	**Fechar**: Botão para fechar a tela de exibição do email;
4.	**Voltar**: Botão de retorno à lista de emails.

